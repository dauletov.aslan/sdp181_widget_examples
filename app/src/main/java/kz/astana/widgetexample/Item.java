package kz.astana.widgetexample;

public class Item {

    private int image;
    private String text;

    public Item(int image, String text) {
        this.image = image;
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public String getText() {
        return text;
    }
}

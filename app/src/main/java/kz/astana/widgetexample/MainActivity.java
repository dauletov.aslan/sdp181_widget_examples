package kz.astana.widgetexample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private AutoCompleteTextView autoCompleteTextView;
    private MultiAutoCompleteTextView multiAutoCompleteTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                new ListData().getData()
        );

        listView.setAdapter(adapter);

        autoCompleteTextView = findViewById(R.id.autoTextView);
        autoCompleteTextView.setAdapter(adapter);
        autoCompleteTextView.setThreshold(1);

        multiAutoCompleteTextView = findViewById(R.id.mutliTextView);

        ArrayAdapter<String> multiAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                new ListData().getDataWithCapital()
        );

        multiAutoCompleteTextView.setAdapter(multiAdapter);
        multiAutoCompleteTextView.setThreshold(1);
        multiAutoCompleteTextView.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
    }

    public void onFabClick(View view) {
        Intent intent = new Intent(MainActivity.this, AdapterActivity.class);
        startActivity(intent);
//        autoCompleteTextView.showDropDown();
//        autoCompleteTextView.dismissDropDown();
    }
}
package kz.astana.widgetexample;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class AdapterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapter);

        ListView listView = findViewById(R.id.listView);

        MyAdapter myAdapter = new MyAdapter(
                AdapterActivity.this,
                new ListData().getDataWithImage(),
                R.layout.layout_adapter
        );

        listView.setAdapter(myAdapter);

        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAdapter.addItem(new Item(R.drawable.ic_serbia, "Serbia"));

                ArrayList<Item> items = new ArrayList<Item>();
                items.add(new Item(R.drawable.ic_albania, "Albania"));
                items.add(new Item(R.drawable.ic_turkey, "Turkey"));
                myAdapter.addItems(items);
                myAdapter.notifyDataSetChanged();
            }
        });

        Spinner spinner = findViewById(R.id.spinner);
        spinner.setAdapter(myAdapter);
    }
}
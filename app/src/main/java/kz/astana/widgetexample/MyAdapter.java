package kz.astana.widgetexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Item> data;
    private int resLayout;

    public MyAdapter(Context context, ArrayList<Item> data, int resLayout) {
        this.context = context;
        this.data = data;
        this.resLayout = resLayout;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = LayoutInflater
                    .from(context)
                    .inflate(resLayout, parent, false);
        }

        ImageView imageView = view.findViewById(R.id.imageView);
        imageView.setImageResource(data.get(position).getImage());
        TextView textView = view.findViewById(R.id.textView);
        textView.setText(data.get(position).getText());

        return view;
    }

    public void addItem(Item item) {
        data.add(item);
    }

    public void addItems(ArrayList<Item> items) {
        data.addAll(items);
    }
}

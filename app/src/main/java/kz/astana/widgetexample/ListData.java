package kz.astana.widgetexample;

import java.util.ArrayList;

public class ListData {

    public ArrayList<String> getData() {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("Kazakhstan");
        arrayList.add("Russia");
        arrayList.add("China");
        arrayList.add("Japan");
        arrayList.add("Korea");
        arrayList.add("England");
        arrayList.add("Germany");
        arrayList.add("Poland");

        return arrayList;
    }

    public ArrayList<String> getDataWithCapital() {
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("Kazakhstan, Nur-Sultan");
        arrayList.add("Russia, Moscow");
        arrayList.add("China, Beijing");
        arrayList.add("Japan, Tokyo");
        arrayList.add("Korea, Seoul");
        arrayList.add("England, London");
        arrayList.add("Germany, Berlin");
        arrayList.add("Poland, Warsaw");

        return arrayList;
    }

    public ArrayList<Item> getDataWithImage() {
        ArrayList<Item> arrayList = new ArrayList<Item>();
        arrayList.add(new Item(R.drawable.ic_kazakhstan, "Kazakhstan"));
        arrayList.add(new Item(R.drawable.ic_russia, "Russia"));
        arrayList.add(new Item(R.drawable.ic_china, "China"));
        arrayList.add(new Item(R.drawable.ic_japan, "Japan"));
        arrayList.add(new Item(R.drawable.ic_south_korea, "Korea"));
        arrayList.add(new Item(R.drawable.ic_england, "England"));
        arrayList.add(new Item(R.drawable.ic_germany, "Germany"));
        arrayList.add(new Item(R.drawable.ic_romania, "Romania"));

        return arrayList;
    }
}
